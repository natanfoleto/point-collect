import React, {useRef} from 'react';
import { Image } from 'react-native';


import logo from '../../assets/logo.png';

import { Container, Form, FormInput, SubmitButton, SignLink, SignLinkText } from './styles';




export default function SignUp({ navigation }) {

  const emailRef = useRef();
  const passwordRef = useRef();
  const confirmPasswordRef = useRef();

  function handleRegister(){

  }

  return (
    <>
      <Container>

        <Image source={logo} />


        <Form>
          <FormInput
            icon="person-outline"
            autoCorrect={false}
            autoCapitalize="none"
            placeholder="Digite seu nome completo"
            returnKeyType="next"
            onSubmitEditing={() => emailRef.current.focus()}
          />

          <FormInput
            icon="mail-outline"
            keyboardType="email-address"
            autoCorrect={false}
            autoCapitalize="none"
            placeholder="Digite seu e-mail"
            ref={emailRef}
            returnKeyType="next"
            onSubmitEditing={() => passwordRef.current.focus()}
          />

          <FormInput
            icon="lock-outline"
            secureTextEntry
            placeholder="Digite sua senha"
            ref={passwordRef}
            returnKeyType="next"
            onSubmitEditing={() => confirmPasswordRef.current.focus()}
          />

          <FormInput
            icon="lock-outline"
            secureTextEntry
            placeholder="Confirme sua senha"
            ref={confirmPasswordRef}
            return="handleRegister"
          />

          <SubmitButton onPress={handleRegister}> CADASTRAR </SubmitButton>

        </Form>

        <SignLink onPress={() => { navigation.navigate('Home') }} >
          <SignLinkText> Já tenho conta </SignLinkText>
        </SignLink>
      </Container>
    </>

  );
}
import styled from 'styled-components/native';


export const ContainerButton = styled.TouchableOpacity`
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

export const Text = styled.Text`
color: #fff;
font-weight: bold;
font-size: 14px;
align-items: center;
justify-content: center;
`;